# 个人记录

## 更新记录

    读写文件指定utf-8编码

## 相关文档说明

- sgns.zhihu.bigram：语义相似度的深度学习模型的预训练的词向量。已储存在阿里云网盘（网盘不支持分分享此类型的文件）
- main.py 执行时间23分钟，生成train.preds.txt

## 使用流程

- 执行build_data.py：使用jieba进行分词，输出分词后的eval.labels.txt和eval.words.txt

- 执行build_embedings.py：文本特征抽取生成w2v.npz
- 执行export.py：导出模型，供测试语义使用
- 执行serve.py: 测试语义是消极还是积极